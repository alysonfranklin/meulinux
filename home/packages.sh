#!/bin/bash

###############################################################
# Instalando browser Opera
sudo echo "deb https://deb.opera.com/opera-stable/ stable non-free #Opera Browser (final releases)" > /etc/apt/sources.list.d/opera-stable.list
sudo apt update
sudo aptitude install opera-stable -y


###############################################################
# Instalando teamviewer
sudo echo -e "deb http://linux.teamviewer.com/deb stable main\ndeb http://linux.teamviewer.com/deb preview main" > /etc/apt/sources.list.d/teamviewer.list
wget -O - https://download.teamviewer.com/download/linux/signature/TeamViewer2017.asc | apt-key add -
sudo apt update
sudo aptitude install teamviewer -y

###############################################################
# Atualizando repositorio:
sudo aptitude update

###############################################################
# Instalando vim
sudo aptitude install vim -y

###############################################################
# Instalando curl e git 
sudo aptitude install git curl -y

###############################################################
# Instalando Tmux
sudo aptitude install tmux -y

###############################################################
# Instalando NeoVim
# https://github.com/neovim/neovim/wiki/Installing-Neovim

sudo aptitude install software-properties-common -y

#If you're using an older version Ubuntu you must use:
#sudo aptitude install python-software-properties

#Run the following commands:

sudo echo -e "#NeoVim\ndeb http://ppa.launchpad.net/neovim-ppa/stable/ubuntu bionic main\ndeb-src http://ppa.launchpad.net/neovim-ppa/stable/ubuntu bionic main" > /etc/apt/sources.list.d/neovim-stable.list

#sudo apt-add-repository ppa:neovim-ppa/stable -y
sudo aptitude update
sudo aptitude install neovim -y

#Prerequisites for the Python modules:
sudo aptitude install python-dev python-pip python3-dev python3-pip -y
pip3 install setuptools

#sudo pip2 install --upgrade --force-reinstall neovim
sudo pip3 install --upgrade --force-reinstall neovim

#If you're using an older version Ubuntu you must use:
#sudo aptitude install python-dev python-pip python3-dev -y
#sudo aptitude install python3-setuptools -y
#sudo easy_install3 pip

#For instructions to install the Python modules, see :help provider-python.

#If you want to use Neovim for some (or all) of the editor alternatives, use the following commands:

#sudo update-alternatives --install $(which vi) vi $(which nvim) 60
#sudo update-alternatives --config vi
sudo update-alternatives --install $(which vim) vim $(which nvim) 60
#sudo update-alternatives --config vim
#sudo update-alternatives --install $(which editor) editor $(which nvim) 60
#sudo update-alternatives --config editor

###############################################################

# Instalando sublime-text

wget -qO - https://download.sublimetext.com/sublimehq-pub.gpg | sudo apt-key add -
sudo aptitude install apt-transport-https -y
sudo echo "deb https://download.sublimetext.com/ apt/stable/" > /etc/apt/sources.list.d/sublime-text.list
sudo aptitude update
sudo aptitude install sublime-text -y

###############################################################

# Instalando Zsh
# Temas: https://github.com/robbyrussell/oh-my-zsh/wiki/themes
apt install zsh -y
cd ~
sudo rm -rf .oh-my-zsh
$ sh -c "$(curl -fsSL https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"

###############################################################

# Instalando Chef Development Kit 3.0.36
wget -c https://packages.chef.io/files/stable/chefdk/3.0.36/ubuntu/18.04/chefdk_3.0.36-1_amd64.deb -O /tmp/chefDk.deb && sudo dpkg -i /tmp/chefDk.deb || aptitude -f install -y

###############################################################

# Instalando docker e docker compose
sudo rm -rf /var/lib/docker
sudo aptitude remove docker docker-engine docker.io -y
sudo curl -sSL https://get.docker.com | sudo sh
sudo usermod -aG docker $(whoami)

#sudo aptitude update
#sudo aptitude install apt-transport-https ca-certificates curl software-properties-common -y
#curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
#sudo apt-key fingerprint 0EBFCD88 -y
#sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu xenial stable" -y
#
#sudo aptitude update
#sudo aptitude install docker-ce docker-compose -y


###############################################################
# Instalando virtualbox
sudo echo "deb https://download.virtualbox.org/virtualbox/debian xenial contrib" > /etc/apt/sources.list.d/virtualbox.list
cd /tmp/ ; wget -q https://www.virtualbox.org/download/oracle_vbox_2016.asc -O- | sudo apt-key add -
sudo aptitude update ; sudo aptitude install virtualbox-5.2 -y
wget -c https://download.virtualbox.org/virtualbox/5.2.12/Oracle_VM_VirtualBox_Extension_Pack-5.2.12.vbox-extpack 
echo "Download do Oracle_VM_VirtualBox_Extension_Pack-5.2.12.vbox-extpack Finalizado"
echo "O arquivo se encontra em: /tmp/"

###############################################################

# Instalando Vagrant
cd /tmp/ ; wget -c https://releases.hashicorp.com/vagrant/2.1.2/vagrant_2.1.2_x86_64.deb && sudo dpkg -i vagrant* || sudo aptitude -f install -y

###############################################################

# Instalando docker
sudo apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D
sudo echo 'deb https://apt.dockerproject.org/repo ubuntu-xenial main' > /etc/apt/sources.list.d/docker.list
sudo aptitude update && sudo aptitude install apt-transport-https ca-certificates curl software-properties-common -y
sudo aptitude update
apt-cache policy docker-engine
sudo aptitude install docker-engine -y
sudo usermod -aG docker $(whoami)
/etc/init.d/docker status
#sudo systemctl status docker

###############################################################

# Instalando ForticlientVPN
cd /tmp/
wget -c https://hadler.me/files/forticlient-sslvpn_4.4.2333-1_amd64.deb && sudo dpkg -i forticlient* || aptitude -f install -y

###############################################################

# Instalando Awscli
sudo apt install awscli -y
pip install awscli --upgrade --user

###############################################################

# Instalando Spotify

sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 0DF731E45CE24F27EEEB1450EFDC8610341D9410 931FF8E79F0876134EDDBDCCA87FF9DF48BF1C90

echo deb http://repository.spotify.com stable non-free > /etc/apt/sources.list.d/spotify.list

sudo aptitude update

sudo aptitude install spotify-client

echo "# NÃO ESQUEÇA DE INSTALAR O TERRAFORM"

###############################################################

echo -e "#Remmina\ndeb http://ppa.launchpad.net/remmina-ppa-team/remmina-next/ubuntu bionic main\ndeb-src http://ppa.launchpad.net/remmina-ppa-team/remmina-next/ubuntu bionic main" > /etc/apt/sources.list.d/remmina.list

sudo aptitude update
sudo aptitude install remmina remmina-plugin-* libfreerdp-plugins-standard -y


###############################################################
